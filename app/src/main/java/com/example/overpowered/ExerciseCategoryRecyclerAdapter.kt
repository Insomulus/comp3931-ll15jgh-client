package com.example.overpowered

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


// exercise category adapter to recycler views for amount of categories that exist
class ExerciseCategoryRecyclerAdapter(categoryList: ArrayList<ExerciseCategory>,exerciseList: ArrayList<ExerciseItem>, context: Context) :
    RecyclerView.Adapter<ExerciseCategoryRecyclerAdapter.CategoryViewHolder>() {
    private var exerciseCategoryList: ArrayList<ExerciseCategory> = categoryList
    private var exerciseItemList: ArrayList<ExerciseItem> = exerciseList
    private var fragmentContext: Context = context




    class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var category: TextView = itemView.findViewById(R.id.exerciseCategory)
        var exerciseRecyclerView: RecyclerView = itemView.findViewById(R.id.searchExerciseRecyclerView)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.exercise__search_category, parent, false)
        return CategoryViewHolder(view)
    }


    override fun getItemCount(): Int {
        return exerciseCategoryList.size
    }

    // binds category name to list then runs through all exercises provided and matches
    // each exercise to their corresponding categories, if belongs to multiple categories, place
    // in all, add list of exercises for current category to list
    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val currentItem: ExerciseCategory = exerciseCategoryList[position]
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(fragmentContext, LinearLayoutManager.HORIZONTAL, false)
        holder.exerciseRecyclerView.layoutManager = layoutManager
        holder.exerciseRecyclerView.setHasFixedSize(true)
        holder.category.text = currentItem.category
        val arrayList: ArrayList<ExerciseItem> = ArrayList()
        Log.d("PRINTING list of item ", exerciseItemList.toString())
        for (i in 0 until exerciseItemList.size) { // 0
            Log.d("PRINTING list of item ", exerciseItemList[i].exerciseCategory.size.toString())
            for (j in 0 until exerciseItemList[i].exerciseCategory.size) { // 6 // 0
                if(exerciseCategoryList[position].category == exerciseItemList[i].exerciseCategory[j]) {
                    Log.d("PRINTING category of item ", exerciseItemList[i].exerciseCategory[j])
                    arrayList.add(exerciseItemList[i])
                }
            }
        }



        Log.d("PRINTING array ", arrayList.toString())

        // pass list of current exercises to exercise item recycler adapter
        val exerciseRecyclerViewAdapter =
            ExerciseItemRecyclerAdapter(arrayList)
            holder.exerciseRecyclerView.adapter = (exerciseRecyclerViewAdapter)


    }

    // old unused
    fun addItem(exercise: ExerciseItem) {
        exerciseItemList.add(0, exercise)
        Log.d("PRINTING NEW item ", exercise.exerciseName.toString())
        Log.d("PRINTING NEW LIST ", exerciseItemList.toString())
        this.notifyItemInserted(0)
    }
}

