package com.example.overpowered

import android.graphics.BitmapFactory
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView


// exercise item adapter that inflates individual exercises within categories
class ExerciseItemRecyclerAdapter(arrayList: ArrayList<ExerciseItem>) :
    RecyclerView.Adapter<ExerciseItemRecyclerAdapter.ExerciseViewHolder>() {
    var exerciseItemArrayList: ArrayList<ExerciseItem> = arrayList
    lateinit var view: View
    // binding required views to variables to be changed later
    class ExerciseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var exerciseImage: ImageView = itemView.findViewById(R.id.exerciseImage)
        var exerciseName: TextView = itemView.findViewById(R.id.exerciseItemName)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseViewHolder {
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.exercise_item, parent, false)
        return ExerciseViewHolder(view)
    }
    // on bind get set from list, and bind data, decode base64 string to bitmap image and set to
    // image view, set on click listener
    override fun onBindViewHolder(holder: ExerciseViewHolder, position: Int) {
        val currentItem: ExerciseItem = exerciseItemArrayList[position]
        val imageBytes = Base64.decode(currentItem.exerciseImage, Base64.DEFAULT)
        val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
        holder.exerciseImage.setImageBitmap(decodedImage)
        holder.exerciseName.text = currentItem.exerciseName
        // on click exercise image switch to exercise information fragment for selected exercise
        holder.exerciseImage.setOnClickListener {
            val fragment: Fragment = ExerciseInformationFragment.newInstance(
                currentItem.exerciseName, decodedImage, currentItem.exerciseDescription,
                currentItem.exerciseInstructions, currentItem.exerciseCategory)
            val fragmentManager = (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.replace(R.id.contentLayout, fragment).commit()
        }
        holder.exerciseName.setOnClickListener {
            Log.d("PRINTING SETLIST ", holder.exerciseName.text.toString())
        }
    }

    // old unused
    override fun getItemCount(): Int {
        return exerciseItemArrayList.size
    }

    // old unused
    fun addItem(exercise: ExerciseItem) {
        exerciseItemArrayList.add(0, exercise)
        Log.d("PRINTING NEW item ", exercise.exerciseName.toString())
        Log.d("PRINTING NEW LIST ", exerciseItemArrayList.toString())
        this.notifyItemInserted(0)
    }

}