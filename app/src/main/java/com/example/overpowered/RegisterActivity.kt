package com.example.overpowered

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputLayout
import khttp.*
import kotlinx.android.synthetic.main.activity_register.*
import kotlinx.android.synthetic.main.activity_register.view.*
import java.text.SimpleDateFormat

// register activity to handle user registration
class RegisterActivity : AppCompatActivity() {
    private var text: String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        // set on click listeners for buttons
        val btnRegister = findViewById<Button>(R.id.registerRegisterBtn)
        btnRegister.setOnClickListener { submitRegister() }

        val btnLogin = findViewById<Button>(R.id.registerLoginBtn)
        btnLogin.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
        // call focus listener functions to detect when the user stops focusing on a field to
        // then validate the field
        userFocusListener()
        passFocusListener()
        emailFocusListener()
        firstFocusListener()
        lastFocusListener()
        weightFocusListener()
        heightFocusListener()
        dobFocusListener()

    }
    // when called, collects the data entered by the user, validates it and then submit
    fun submitRegister() {
        val user = findViewById<TextInputLayout>(R.id.usernameContainer)
        text = user?.username?.text.toString()
        user?.helperText = user?.let { userValidityChecker(it.username) }
        val validUser = user?.helperText == null
        val passContainer = findViewById<TextInputLayout>(R.id.passwordContainer)
        text = passContainer?.password?.text.toString()
        passContainer?.helperText = passContainer?.let { passValidityChecker(it.password) }
        val validPass = passContainer?.helperText == null
        val emailContainer = findViewById<TextInputLayout>(R.id.emailContainer)
        text = emailContainer?.email?.text.toString()
        emailContainer?.helperText = emailContainer?.let { emailValidityChecker(it.email) }
        val validEmail = emailContainer?.helperText == null
        val firstContainer = findViewById<TextInputLayout>(R.id.firstNameContainer)
        text = firstContainer?.firstName?.text.toString()
        firstContainer?.helperText = firstContainer?.let { firstValidityChecker(it.firstName) }
        val validFirst = firstContainer?.helperText == null
        val lastContainer = findViewById<TextInputLayout>(R.id.lastNameContainer)
        text = lastContainer?.lastName?.text.toString()
        lastContainer?.helperText = lastContainer?.let { lastValidityChecker(it.lastName) }
        val validLast = lastContainer?.helperText == null
        val heightContainer = findViewById<TextInputLayout>(R.id.heightContainer)
        text = heightContainer?.height2.toString()
        heightContainer?.helperText = heightContainer?.let { heightValidityChecker(it.height2) }
        val validHeight = heightContainer?.helperText == null
        val weightContainer = findViewById<TextInputLayout>(R.id.weightContainer)
        text = weightContainer?.weight.toString()
        weightContainer?.helperText = weightContainer?.let { weightValidityChecker(it.weight) }
        val validWeight = weightContainer?.helperText == null
        val dobContainer = findViewById<TextInputLayout>(R.id.dobContainer)
        text = dobContainer?.dateOfBirth.toString()
        dobContainer?.helperText = dobContainer?.let { dobValidityChecker(it.dateOfBirth) }
        val validDob = dobContainer?.helperText == null

        // if valid then submit register details
        if (validUser && validPass && validDob
            && validEmail && validFirst && validLast
            && validHeight && validWeight) {
            submitRegisterDetails()
        }
        else {
            // if validation fails, warn the user
            invalidDetails()
        }

    }

    // warns the user via dialog that the details are incorrect
    private fun invalidDetails() {
        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle("Error Logging in")
            setMessage("You have entered some incorrect values, please try again.")
            setNegativeButton("Retry") { dialog, _ ->
                dialog.cancel()
            }
            show()
        }
    }
    // stores user entered data and tries to connect to the API end points
    private fun submitRegisterDetails() {
        val firstName = findViewById<EditText>(R.id.firstName).text.toString()
        val lastName = findViewById<EditText>(R.id.lastName).text.toString()
        val height = findViewById<EditText>(R.id.height2).text.toString()
        val weight = findViewById<EditText>(R.id.weight).text.toString()
        val dateOfBirth = findViewById<EditText>(R.id.dateOfBirth).text.toString()
        val username = findViewById<EditText>(R.id.username).text.toString()
        val email = findViewById<EditText>(R.id.email).text.toString()
        val password = findViewById<EditText>(R.id.password).text.toString()
        try {
            // creates a JSON object containing user account details
            val payload = mapOf("first_name" to firstName, "last_name" to lastName, "height" to height, "weight" to weight, "date_of_birth" to dateOfBirth, "username" to username, "email" to email, "password" to password )
            val r = post("http://192.168.0.15:8000/api/register/", data=payload)
            println(payload)
            println(r.statusCode)
            println(r.text)
            println(r.statusCode)
            // if the user exists already, return error and notify
            if (r.statusCode == 401) {
                userExists()
                println("ok")
                findViewById<EditText>(R.id.username).setText("")
            }
            // if the email exists already, return error and notify
            else if (r.statusCode == 402) {
                emailExists()
                println("ok")
                findViewById<EditText>(R.id.email).setText("")
            }
            // if the successful, redirect user to Login Activity
            else if (r.statusCode == 200) {
                val intent = Intent(this, LoginActivity::class.java)
                startActivity(intent)
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
    // error dialog if user exists
    private fun userExists() {
        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle("Error registering account")
            setMessage("That username already exists, please pick another.")
            setNegativeButton("Retry") { dialog, _ ->
                dialog.cancel()
            }
            show()
        }
    }
    // error dialog if email exists
    private fun emailExists() {
        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle("Error registering account")
            setMessage("That email already exists, please pick another.")
            setNegativeButton("Retry") { dialog, _ ->
                dialog.cancel()
            }
            show()
        }
    }
    // if user stops focusing on this field then validate the contents
    private fun userFocusListener()
    {
        username.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                usernameContainer.helperText = userValidityChecker(username)
            }
        }
    }
    // verify that the entry is not empty
    private fun userValidityChecker(editText: TextView): String?
    {
        val user = editText.text.toString()
        if(user.isEmpty())
        { return "Please enter a username."

        }
        return null
    }

    private fun emailFocusListener()
    {
        email.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                emailContainer.helperText = emailValidityChecker(email)
            }
        }
    }
    private fun emailValidityChecker(editText: TextView): String?
    {
        val email = editText.text.toString()
        if(email.isEmpty()) {
            return "Please enter an email."
        }
        else if (!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            return null
        }
        else {
            return "Please enter a valid email."
        }

    }

    private fun firstFocusListener()
    {
        firstName.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                firstNameContainer.helperText = firstValidityChecker(firstName)
            }
        }
    }
    private fun firstValidityChecker(editText: TextView): String?
    {
        val first = editText.text.toString()
        if(first.isEmpty())
        { return "Please enter a first name."

        }
        return null
    }

    private fun lastFocusListener()
    {
        lastName.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                lastNameContainer.helperText = lastValidityChecker(lastName)
            }
        }
    }
    private fun lastValidityChecker(editText: TextView): String?
    {
        val last = editText.text.toString()
        if(last.isEmpty())
        { return "Please enter a last name."

        }
        return null
    }

    private fun heightFocusListener()
    {
        height2.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                heightContainer.helperText = heightValidityChecker(height2)
            }
        }
    }
    private fun heightValidityChecker(editText: TextView): String?
    {
        val height = editText.text.toString()
        if(height.isEmpty())
        { return "Please enter a height."
        }
        else if(height.length > 3)
        { return "Please enter a valid height, you're not that tall..."

        }
        return null
    }

    private fun weightFocusListener()
    {
        weight.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                weightContainer2.helperText = weightValidityChecker(weight)
            }
        }
    }
    private fun weightValidityChecker(editText: TextView): String?
    {
        val weight = editText.text.toString()
        if(weight.isEmpty())
        { return "Please enter a weight."
        }
        else if(weight.length > 3)
        { return "Please enter a valid weight, you're not that heavy..."

        }
        return null
    }

    private fun dobFocusListener()
    {
        dateOfBirth.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                dobContainer.helperText = dobValidityChecker(dateOfBirth)
            }
        }
    }
    private fun dobValidityChecker(editText: TextView): String?
    {
        val dob = editText.text.toString()
        if(dob.isEmpty())
        { return "Please enter a date of birth."
        }
        else {
            try {
                val date = SimpleDateFormat("YYYY-MM-DD").parse(dob)
                return null
            } catch (ex: java.lang.Exception) {
                return "Please enter date in correct format YYYY-MM-DD"
            }
        }
    }


    private fun passFocusListener()
    {
        password.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                passwordContainer.helperText = passValidityChecker(password)
            }
        }
    }
    private fun passValidityChecker(editText: TextView): String?
    {
        val pass = editText.text.toString()
        if(pass.isEmpty())
        { return "Please enter a password"
        }
        else if(pass.length < 8)
        {
            return "Password cannot be less than 8 characters"
        }

        return null
    }
}

