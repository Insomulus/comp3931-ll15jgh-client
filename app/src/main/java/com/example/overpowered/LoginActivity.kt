package com.example.overpowered

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import androidx.appcompat.app.AppCompatActivity
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.google.android.material.textfield.TextInputLayout
import khttp.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_login.view.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter


// login activity to handle user login, sessions and authentication
class LoginActivity : AppCompatActivity() {
    private var username: String? = null
    private var password: String? = null
    lateinit var session: Session
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        // check if user is set to logged in via session key, if so then redirect to Main Activity
        session = Session(applicationContext)
        if (session.isLoggedIn()){
            val i = Intent(applicationContext,MainActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(i)
            finish()
        }
        // permit thread policy, prevents error messages being thrown about network processes on UI
        // thread, if creating production software, remove this and handle threading correctly
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        setContentView(R.layout.activity_login)

        // on click listeners for buttons
        val login  = findViewById<Button>(R.id.loginBtn)
        login.setOnClickListener { submitLogin() }

        val register  = findViewById<Button>(R.id.registerBtn)
        register.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }
        // more focus listeners for validation
        userFocusListener()
        passFocusListener()

    }
    // run validity checks on all fields
    fun submitLogin() {
        val userContainer = findViewById<TextInputLayout>(R.id.userContainer)
        username = userContainer?.userText?.text.toString()
        userContainer?.helperText = userContainer?.let { userValidityChecker(it.userText) }
        val validUser = userContainer?.helperText == null
        val passContainer = findViewById<TextInputLayout>(R.id.loginPassContainer)
        password = passContainer?.loginPassText?.text.toString()
        passContainer?.helperText = passContainer?.let { passValidityChecker(it.loginPassText) }
        val validPass = passContainer?.helperText == null
        // if successful, attempt to submit login details to server
        if (validUser && validPass) {
            submitLoginDetails()
        }
        // else notify user of error
        else {
            invalidDetails()
        }

    }
    // invalid details provided dialog
    private fun invalidDetails() {
        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle("Error Logging in")
            setMessage("You have entered some incorrect values, please try again.")
            setNegativeButton("Retry") { dialog, _ ->
                dialog.cancel()
            }
            show()
        }
    }

    // submits login details to API end point
    private fun submitLoginDetails() {
        val username = findViewById<EditText>(R.id.userText).text.toString()
        val password = findViewById<EditText>(R.id.loginPassText).text.toString()
        try {
            // post request with a JSON payload including login details
            val payload = mapOf("username" to username, "password" to password)
            val r = post("http://192.168.0.15:8000/api/login/", data = payload)
            println(r.statusCode)
            // if user doesn't exist, warn user
            if (r.statusCode == 401) {
                userNotExist() }
            // if successful, set the current date and store it along with the username, launch
            // Main Activity
            else if (r.statusCode == 200) {
                val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
                val rawDate = LocalDate.now()
                val formattedLocalDate = rawDate.format(formatter)
                session.createSession(username, formattedLocalDate)
                val i = Intent(applicationContext, MainActivity::class.java)
                startActivity(i)
                finish()
            }   // Try send details
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    // if user doesn't exist, ask to try again or redirect to register
    private fun userNotExist() {
        val builder = AlertDialog.Builder(this)
        with(builder)
        {
            setTitle("Error Logging in")
            setMessage("That username does not exist, please try again or register an account.")
            setPositiveButton("Register") { dialog, _ ->
                val intent = Intent(applicationContext, RegisterActivity::class.java)
                startActivity(intent)
            }
            setNegativeButton("Retry") { dialog, _ ->
                dialog.cancel()
            }
            show()
        }
    }
    // focus listeners for fields, validation checked once field is removed from focus
    private fun userFocusListener()
    {
        userText.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                userContainer.helperText = userValidityChecker(userText)
            }
        }
    }
    // validity checker for fields
    private fun userValidityChecker(editText: TextView): String?
    {
        val user = editText.text.toString()
        if(user.isEmpty())
        { return "Please enter a username."

        }
        return null
    }

    private fun passFocusListener()
    {
        loginPassText.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                loginPassContainer.helperText = passValidityChecker(loginPassText)
            }
        }
    }
    private fun passValidityChecker(editText: TextView): String?
    {
        val pass = editText.text.toString()
        if(pass.isEmpty())
        { return "Please enter a password"
        }
        else if(pass.length < 8)
        {
            return "Password cannot be less than 8 characters"
        }

        return null
    }
}
