package com.example.overpowered

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.widget.DatePicker
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import java.util.*


// the Date Picker Fragment that launches a calendar instance and passes the chosen date to the
// other fragments
class DatePickerFragment : DialogFragment(), OnDateSetListener {
    var dateModel: DateSharedViewModel? = null
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        // uses current date as default start date on calendar, in future update, start date should
        // currently selected date
        val c = Calendar.getInstance()
        val year = c[Calendar.YEAR]
        val month = c[Calendar.MONTH]
        val day = c[Calendar.DAY_OF_MONTH]

        // create instance and return it
        return DatePickerDialog(requireActivity(), this, year, month, day)
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, day: Int) {

        // when date is selected, adjust string storing date value in DateFragment
        Log.d("SUCCESS", (day.toString()))
        Log.d("SUCCESS", (month.toString()))
        Log.d("SUCCESS", (year.toString()))
        var adjustedDay = day.toString()
        val increaseMonth = month + 1
        var adjustedMonth = increaseMonth.toString()

        if (day < 10) {
            adjustedDay = "0$adjustedDay"
        }
        if (month < 10) {
            adjustedMonth = "0$adjustedMonth"
        }
        val date = "$adjustedDay-$adjustedMonth-$year"

        Log.d("SUCCESS", (date))

        // had var defined here instead previously, if find issues in future then revert
        dateModel = ViewModelProvider(requireActivity())[DateSharedViewModel::class.java]
        dateModel!!.store(date)


    }

}