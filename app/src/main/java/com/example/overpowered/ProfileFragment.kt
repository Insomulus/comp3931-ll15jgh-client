package com.example.overpowered

import android.Manifest
import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.PermissionChecker.PERMISSION_GRANTED
import androidx.fragment.app.Fragment
import com.example.overpowered.Session.Companion.KEY_LATITUDE
import com.example.overpowered.Session.Companion.KEY_LONGITUDE
import com.example.overpowered.Session.Companion.KEY_USERNAME
import com.google.android.gms.location.FusedLocationProviderClient
import khttp.get
import kotlinx.android.synthetic.main.fragment_profile.*
import org.json.JSONException
import org.json.JSONObject
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.events.MapEventsReceiver
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.overlay.MapEventsOverlay
import org.osmdroid.views.overlay.Marker
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay
import java.lang.Exception
import kotlin.collections.HashMap


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ProfileFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

// profile fragment shows profile details and allows user to select their gym location on map
class ProfileFragment : Fragment(), MapEventsReceiver {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    lateinit var session: Session
    lateinit var details: HashMap<String?, String?>
    private lateinit var map1: org.osmdroid.views.MapView
    private lateinit var locationOverlay: MyLocationNewOverlay
    private lateinit var mapController: IMapController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        session = Session(this.requireActivity())
        details = session.getAccountDetails()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // makes sure that the user has provided permissions to access location, if granted draw map, if not request permissions
        if (ContextCompat.checkSelfPermission(requireActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PERMISSION_GRANTED) {
            permissionsGranted()
        } else {
            ActivityCompat.requestPermissions(requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
        }
        // pull user account details
        pullDetails()
        // on click map button hide everything and reveal map
        mapBtn.setOnClickListener {
            emailContainer2.visibility = GONE
            firstNameContainer2.visibility = GONE
            lastNameContainer2.visibility = GONE
            heightContainer2.visibility = GONE
            weightContainer22.visibility = GONE
            profileBackBtn.visibility = GONE
            saveChangesBtn.visibility = GONE
            mapBtn.visibility = GONE
            addressTxt.visibility = GONE
            mappyboi.visibility = VISIBLE
        }
        // on click logout button log the user out and process logout request with server
        loogoutBtn.setOnClickListener {
            (activity as MainActivity).logoutRequest()
        }
     }

    // attempt to pull user account details from server
    private fun pullDetails() {
        try {
            // connect to API end point with username and attempt to pull related user details
            val payload = mapOf("username" to details[KEY_USERNAME])
            val r = get("http://192.168.0.15:8000/api/pull_user_details/", data = payload)
            // if successful then process user details
            if (r.statusCode == 200) {
                val details = r.jsonObject.getJSONObject("details")
                processDetails(details)
            } else if (r.statusCode == 404){
                // if unsuccessful warn user
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error Pulling Details")
                    setMessage("Apparently you don't exist....")
                    setNegativeButton("Exit") { dialog, _ ->
                        dialog.cancel()
                    }
                    setPositiveButton("Retry") { dialog, _ ->
                        pullDetails()
                    }
                    show()
                }
            }
            else
            {
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error Pulling Exercises")
                    setMessage("Trouble contacting server, please try again later.")
                    setNegativeButton("Exit") { dialog, _ ->
                        dialog.cancel()
                    }
                    setPositiveButton("Retry") { dialog, _ ->
                        pullDetails()
                    }
                    show()
                }

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    // put user details into displayed fields
    fun processDetails(details: JSONObject) {
        getAddress()
            val currentDetail = details
            editProfile.text = currentDetail.get("username").toString()
            email2.hint = currentDetail.get("email").toString()
            firstName2.hint = currentDetail.get("first_name").toString()
            lastName2.hint = currentDetail.get("last_name").toString()
            height22.hint = currentDetail.get("height").toString()
            weight2.hint = currentDetail.get("weight").toString()

    }

    // passes the users current coordinates to server and returns an address
    fun getAddress(){
        try {
            val payload = mapOf("latitude" to details[KEY_LATITUDE], "longitude" to details[KEY_LONGITUDE])
            val r = get("http://192.168.0.15:8000/api/get_location/", data = payload)
            // if successful then set address text to address provided
            if (r.statusCode == 200) {
                addressTxt.text = r.toString()
                // if unsuccessful warn user
            } else if (r.statusCode == 404){
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error Pulling Address")
                    setMessage("Server error...")
                    setNegativeButton("Exit") { dialog, _ ->
                        dialog.cancel()
                    }
                    setPositiveButton("Retry") { dialog, _ ->
                        getAddress()
                    }
                    show()
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }



override fun onResume() {
        super.onResume()
        map1.onResume()
    }

    override fun onPause() {
        super.onPause()
        map1.onPause()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ProfileFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ProfileFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    // if permissions were granted then setup map and apply settings, create overlays
    fun permissionsGranted() {
        map1 = requireView().findViewById(R.id.mappyboi)
        map1.setTileSource(TileSourceFactory.MAPNIK)
        map1.setMultiTouchControls(true)
        map1.zoomController?.setVisibility(CustomZoomButtonsController.Visibility.NEVER)
        Configuration.getInstance().userAgentValue = requireActivity().packageName
        mapController = map1.controller!!
        locationOverlay = MyLocationNewOverlay(GpsMyLocationProvider(requireContext()), map1)
        locationOverlay.enableMyLocation()
        locationOverlay.enableFollowLocation()
        locationOverlay.isDrawAccuracyEnabled = true
        // animates the map moving to user location
        locationOverlay.runOnFirstFix{requireActivity().runOnUiThread {
            mapController.animateTo(locationOverlay.myLocation)
            mapController.setZoom(6.2)
        }}
        // add overlay to map
        map1.overlays?.add(locationOverlay)
        val mapEventsOverlay = MapEventsOverlay(requireActivity(), this)
        // add another overlay for when the user clicks on the map
        map1.overlays.add(0, mapEventsOverlay)


    }

    // if single tap on map do nothing
    override fun singleTapConfirmedHelper(location: GeoPoint?): Boolean {
        Log.d("PRINTED", location.toString())
        return true
    }

    // if long press on map then save coordinates of click location
    override fun longPressHelper(location: GeoPoint?): Boolean {
        Log.d("PRINTED", location.toString())
        // places a marker at selected location
        val marker = Marker(map1)
        marker.position = GeoPoint(location)
        marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
        marker.icon = resources.getDrawable(R.drawable.ic_baseline_my_location_24)
        marker.setInfoWindow(null)
        map1.overlays.add(marker)
        map1.invalidate()
        // save current location as hoome gym
        saveGym(location)
        return true

    }

    // saves provided coordinates as user home gym
    private fun saveGym(location: GeoPoint?) {
        val builder = AlertDialog.Builder(context)
        with(builder)
        {
            setTitle("Saving Gym Location")
            setMessage("Are you sure you want to save this as your home gym and return to profile?")
            setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }
            setPositiveButton("Yes") { dialog, _ ->
                // if location provided isn't null then
                if (location != null) {
                    // set session coordinates to selected location
                    session.changeGymLocation(location.latitude, location.longitude)

                    Log.d("PRINTED", location.latitude.toString() + " " + location.longitude.toString())
                    Log.d("PRINTED", details[Session.KEY_LATITUDE.toString()] + details[Session.KEY_LONGITUDE.toString()])
                    dialog.cancel()
                    // get new address for new gym
                    getAddress()
                    // remove map and make other UI elements visible
                    emailContainer2.visibility = VISIBLE
                    firstNameContainer2.visibility = VISIBLE
                    lastNameContainer2.visibility = VISIBLE
                    heightContainer2.visibility = VISIBLE
                    weightContainer22.visibility = VISIBLE
                    profileBackBtn.visibility = VISIBLE
                    saveChangesBtn.visibility = VISIBLE
                    mapBtn.visibility = VISIBLE
                    addressTxt.visibility = VISIBLE
                    mappyboi.visibility = GONE
                }
            }
            show()
        }
    }
}