package com.example.overpowered

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log

// getters, setters and and cleaners for my Session,
// session class stores username, login status, date, coordinates an current local lost of exercise and set items
class Session(private var con: Context) {
    var preferences: SharedPreferences
    var editor: SharedPreferences.Editor
    var PRIVATE_MODE: Int = 0

    init {
        // init shared preferences and shared preferences editor
        preferences = con.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = preferences.edit()
    }

    // session name and associated variables
    companion object {
        var PREF_NAME: String = "Session"
        var IS_LOGIN: String = "logged?"
        var KEY_USERNAME: String = "username"
        var KEY_DATE: String = "date"
        var KEY_LATITUDE: String = "latitude"
        var KEY_LONGITUDE: String = "longitude"
        lateinit var KEY_EXERCISES: ExerciseInstance
        lateinit var KEY_SETS: SetInstance
    }

    // when called, create a session and store current date, username and login status
    fun createSession(name: String, date: String) {
        editor.putBoolean(IS_LOGIN, true)
        editor.putString(KEY_USERNAME, name)
        editor.putString(KEY_DATE, date)
        editor.commit()
        val user: HashMap<String?, String?> = getAccountDetails()
        println(user[KEY_DATE])
    }
    // function to call when the date is changed throughout the application
    fun changeDate(date: String) {
        editor.putString(KEY_DATE, date)
        editor.commit()
    }
    // function to call when the location is changed
    fun changeGymLocation(latitude: Double, longitude: Double) {
        editor.putString(KEY_LATITUDE, latitude.toString())
        editor.putString(KEY_LONGITUDE, longitude.toString())
        editor.commit()
    }

    fun changeExercises(exercise: ExerciseInstance) {
        KEY_EXERCISES = exercise
        editor.commit()
    }

    // old depreciated functions that are no longer used
    fun changeSet(set: SetInstance) {
        KEY_SETS = set
        editor.commit()
        Log.d("test ", KEY_SETS.toString())
    }

    fun getExercise(exercise: ExerciseInstance): ExerciseInstance {
        return KEY_EXERCISES
    }

    fun getSet(set: SetInstance): SetInstance {
        return KEY_SETS
    }
    fun getLatitude(): Double {
        return KEY_LATITUDE.toDouble()
    }
    fun getLongitude(): Double {
        return KEY_LONGITUDE.toDouble()
    }
    // checks that the user is set to logged in within the session, if so, launch login activity
    fun auth() {
        if (!this.isLoggedIn()) {
            val i = Intent(con, LoginActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            con.startActivity(i)
        }

    }

    // return stored session details related to current account
    fun getAccountDetails(): HashMap<String?, String?> {
        val user: Map<String?,String?> = HashMap()
        (user as HashMap)[KEY_USERNAME] = preferences.getString(KEY_USERNAME,null)
        user[KEY_DATE] = preferences.getString(KEY_DATE,null)
        user[KEY_LONGITUDE] = preferences.getString(KEY_LONGITUDE,null)
        user[KEY_LATITUDE] = preferences.getString(KEY_LATITUDE,null)
        return user

    }

    // ends the current session and clears file on local storage
    fun endSession() {
        editor.clear()
        editor.commit()

        val i = Intent(con,LoginActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        con.startActivity(i)
    }

    // returns true if user is logged in for login checker
    fun isLoggedIn(): Boolean {
        return preferences.getBoolean(IS_LOGIN, false)
    }
}
