package com.example.overpowered

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.overpowered.Session.Companion.KEY_DATE
import com.example.overpowered.Session.Companion.KEY_USERNAME
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import khttp.get
import org.json.JSONArray
import org.json.JSONException




lateinit var exerciseInstanceRecyclerView: RecyclerView
private lateinit var exerciseInstanceAdapter: RecyclerView.Adapter<*>
var exerciseInstanceArrayList: ArrayList<ExerciseInstance> = ArrayList()
var setInstanceArrayList: ArrayList<SetInstance> = ArrayList()
private lateinit var exerciseInstanceLayout: RecyclerView.LayoutManager
private lateinit var fusedLocationClient: FusedLocationProviderClient

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DiaryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

// diary fragment that displays the users set and exercise information for selected date
class DiaryFragment : Fragment() {
    // TODO: Rename and change types of parameters
    // init session
    private var param1: String? = null
    private var param2: String? = null
    lateinit var session: Session
    lateinit var details: HashMap<String?, String?>
    lateinit var user: String
    lateinit var sessionDate: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        session = Session(this.requireContext())
        details = session.getAccountDetails()
        sessionDate = details[KEY_DATE].toString()
        user = details[KEY_USERNAME].toString()
        // get current user location to compare against selected gym location
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireActivity())
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_diary, container, false)
    }

    // clear stored exercise and set list and pull new ones, once pulled pass to adapter
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clearList()
        pullExercises()
        exerciseInstanceRecyclerView = view.findViewById(R.id.exerciseInstanceRecyclerView)
        exerciseInstanceRecyclerView.setHasFixedSize(true)
        exerciseInstanceLayout = LinearLayoutManager(context)
        exerciseInstanceAdapter = ExerciseInstanceRecyclerAdapter(exerciseInstanceArrayList, setInstanceArrayList, requireContext())
        exerciseInstanceRecyclerView.layoutManager = exerciseInstanceLayout
        exerciseInstanceRecyclerView.adapter = exerciseInstanceAdapter

    }
    // function to clear both lists when called
    private fun clearList() {
        exerciseInstanceArrayList.clear()
        setInstanceArrayList.clear()
    }

    // try to pull exercises from server and dataabse
    private fun pullExercises() {
        try {
            // pass payload to API end point
            val payload = mapOf("username" to user, "date" to sessionDate)
            val r = get("http://192.168.0.15:8000/api/pull_sets/", data = payload)
            // if successful, seperate instances and sets then send both to processExercises function
            if (r.statusCode == 200) {
                val instances = r.jsonObject.getJSONArray("exercise_instances")
                val sets = r.jsonObject.getJSONArray("sets")
                processExercises(instances, sets)
                // if unsuccessful, launch DiaryNoExercise fragment
            } else if (r.statusCode == 404){
                val fragment: Fragment = DiaryNoExercise()
                val fragmentManager = (view?.context as FragmentActivity).supportFragmentManager.beginTransaction()
                activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
                fragmentManager.replace(R.id.contentLayout, fragment, "NOEXERCISE").commit()
                Log.d("404", r.statusCode.toString())
            }
            // else the servers cannot be connected to, present error dialog
            else
             {
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error Pulling Exercises")
                    setMessage("Trouble contacting server, please try again later.")
                    setNegativeButton("Exit") { dialog, _ ->
                        dialog.cancel()
                    }
                    setPositiveButton("Retry") { dialog, _ ->
                        pullExercises()
                    }
                    show()
                }

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    // pulls elements from teh JSONArrays and adds to separate array lists
    private fun processExercises(instances: JSONArray, sets: JSONArray) {
        for (i in 0 until instances.length()) {
            val currentInstance = instances.getJSONObject(i)
            val instanceTime = currentInstance.get("time").toString()
            val instanceId = currentInstance.get("instance_id").toString()
            val instanceExercise = currentInstance.get("exercise").toString()
            // create item from pulled elements and add to list
            exerciseInstanceArrayList.add(ExerciseInstance(instanceTime, instanceId, instanceExercise))
        }
        for (i in 0 until sets.length()) {
            val currentExercise = sets.getJSONObject(i)
            val setTime = currentExercise.get("time").toString()
            val setWeight = currentExercise.get("weight").toString()
            val setResistance = currentExercise.get("resistance").toString()
            val setBodyWeight = currentExercise.get("bodyweight").toString()
            val setAddedWeight = currentExercise.get("added_weight").toString()
            val setReps = currentExercise.get("reps").toString()
            val setInstanceId = currentExercise.get("instance_id").toString()
            val setId = currentExercise.get("set_id").toString()
            // create item from pulled elements and add to list
            setInstanceArrayList.add(SetInstance(setTime, setWeight, setResistance, setBodyWeight,
            setAddedWeight, setReps, setInstanceId, setId))
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DiaryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DiaryFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}