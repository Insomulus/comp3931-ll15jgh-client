package com.example.overpowered

// set object
data class Set(
    val time :String,
    val weight: Int,
    val reps: Int
)
