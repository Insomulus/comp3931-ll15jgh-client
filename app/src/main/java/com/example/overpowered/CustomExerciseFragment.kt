package com.example.overpowered

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.Intent.ACTION_PICK
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.example.overpowered.databinding.FragmentCustomExerciseBinding
import com.google.android.material.chip.Chip
import com.google.android.material.textfield.TextInputLayout
import khttp.post
import kotlinx.android.synthetic.main.fragment_custom_exercise.*
import kotlinx.android.synthetic.main.fragment_custom_exercise.view.*
import kotlinx.android.synthetic.main.fragment_edit_set.view.editBtnEx
import kotlinx.android.synthetic.main.fragment_edit_set.view.weightEditText
import org.json.JSONException
import java.io.ByteArrayOutputStream


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CustomExerciseFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

// custom exercise fragment to allow users to create their own exercises
class CustomExerciseFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var param3: String? = null
    private lateinit var param4: List<String>
    private var param5: String? = null
    private var param6: String? = null
    private var imageBytes = ""
    private var _binding: FragmentCustomExerciseBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
            param3 = ""
            param4 = emptyList()
            param5 = ""
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_custom_exercise, container, false)
    }

    val TRY_SAVE = 0

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentCustomExerciseBinding.inflate(layoutInflater)
        // enable focus listeners for inputs
        nameFocusListener(weightContainer, weightEditText)
        focusListener(exerciseDescriptionContainer, exerciseDescTxtInput)
        focusListener(exerciseInstrContainer, exerciseInstrTxtInput)

        // on click to attempt to save exercise
        view.editBtnEx.setOnClickListener {
            saveEdit()
        }
        // on click back to return to previous fragment
        view.backButtonExercise.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
            val fragment: Fragment = ExerciseSearchFragment()
            val fragmentManager = (context as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.replace(R.id.contentLayout, fragment, "SEARCH").commit()
        }

        // on click camera button to take picture
        view.cameraBtn.setOnClickListener {
            dispatchTakePictureIntent()
        }
        // on click image button to view gallery
        view.imageBtn.setOnClickListener {
            openImageGallery()
        }
    }

    // attempt to save the custom exercise
    fun saveEdit() {
        // check for validity of user input
        val nameContainer = weightContainer
        param1 = nameContainer.weightEditText.text.toString()
        nameContainer.helperText =
            nameContainer?.let { nameValidityChecker(it.weightEditText) }
        val validName = nameContainer.helperText == null
        val descriptionContainer = exerciseDescriptionContainer
        param2 = descriptionContainer.exerciseDescTxtInput.text.toString()
        descriptionContainer.helperText =
            descriptionContainer?.let { nameValidityChecker(it.exerciseDescTxtInput) }
        val validDesc = descriptionContainer.helperText == null
        val instrContainer = exerciseInstrContainer
        param3 = instrContainer.exerciseInstrTxtInput.text.toString()
        instrContainer.helperText =
            instrContainer?.let { nameValidityChecker(it.exerciseInstrTxtInput) }
        val validInstr = instrContainer.helperText == null
        param5 = spinner.selectedItem.toString()

        // if all inputs are valid continue
        if (validName && validDesc && validInstr) {
            // if category chip isn't selected then show error
            if (checkCategory() == null) {
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error")
                    setMessage("You haven't selected a category.")
                    setNegativeButton("No") { dialog, _ ->
                        dialog.cancel()
                    }
                    show()
                }
            }
            // if no image has been provided via camera or gallery button then show error
            else if (imageBytes.isEmpty()) {
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error")
                    setMessage("You haven't added an image, please use the camera or gallery to do so.")
                    setNegativeButton("No") { dialog, _ ->
                        dialog.cancel()
                    }
                    show()
                }
            }
            // if all is fine then submit custom exercise
            else {
                submitEdit()
            }
        } else {
            invalidEdit()
        }

    }

    val REQUEST_IMAGE_CAPTURE = 1
    val PICK_IMAGE = 100

    // launches the camera and allows the user to take a picture
    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        try {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        } catch (e: ActivityNotFoundException) {
            // display error state to the user
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // if request code matches image capture and the result is successful then store image as bitmap
        // convert bitmap to bytearray, compress if set by us and then store base64 string, then
        // finalise by setting image on fragment to user provided image and make it visible
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            val imageBitmap = data?.extras?.get("data") as Bitmap
            val byteArrayOutputStream = ByteArrayOutputStream()
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            imageBytes = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT)
            view?.createExerciseImg?.setImageBitmap(imageBitmap)
            view?.createExerciseImg!!.visibility = VISIBLE
        }
        // if request code matches image gallery and the result is successful then store image as bitmap
        // convert to bytearray and then store base64 string, then finalise by setting image on
        // fragment to user provided image and make it visible
        if (resultCode == RESULT_OK && requestCode == PICK_IMAGE) {
            val imageUri = data?.data as Uri
            view?.createExerciseImg?.setImageURI(imageUri)
            view?.createExerciseImg!!.visibility = VISIBLE
            val inputStream = requireContext().contentResolver.openInputStream(imageUri)
            val byteArray = inputStream?.readBytes()
            imageBytes = Base64.encodeToString(byteArray, Base64.DEFAULT)
        }
    }

    // launch the gallery and allow the user to select a picture
    private fun openImageGallery() {
        val gallery = Intent(ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
        startActivityForResult(gallery, PICK_IMAGE)
    }

    // check the chips within chip group to see if they have been selected or not, store the list
    // of selected chips, each exercise can belong to multiple categories
    private fun checkCategory(): List<String>? {
        val ids = catChipGroup.checkedChipIds
        var catList = emptyList<String>()
        return if (ids.isEmpty()) {
            null
        } else {
            for (id in ids) {
                catList = catList + catChipGroup.findViewById<Chip>(id).text.toString()
            }
            param4 = catList
            return catList
        }
    }


    // confirm user wishes to save custom exercise
    private fun submitEdit() {
        val builder = AlertDialog.Builder(context)
        with(builder)
        {
            setTitle("Creating Exercise")
            setMessage("Are you sure you want to create this exercise?")
            setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }
            setPositiveButton("Yes") { dialog, _ ->
                requestEdit()
            }
            show()
        }
    }

    // request save custom exercise to server
    private fun requestEdit() {
        try {
            // try pass parameters to API end point
            val payload = mapOf("name" to param1, "description" to param2, "instructions" to param3,
            "categories" to param4, "type" to param5, "image" to imageBytes)
            Log.d("CLICK CHECKER", payload.toString())
            val r = post("http://192.168.0.15:8000/api/create_exercise/", data = payload)
            // if successful then store new exercise in database and return to search fragment with
            // new exercise shown
            if (r.statusCode == 200) {
                activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
                val fragment: Fragment = ExerciseSearchFragment()
                val fragmentManager = (context as FragmentActivity).supportFragmentManager.beginTransaction()
                fragmentManager.replace(R.id.contentLayout, fragment, "SEARCH").commit()
                val newItem = ExerciseItem(param1.toString(), param2.toString(), param3.toString(),
                    imageBytes, param5.toString(), param4)

            } else {
                // if unsuccesful show error
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error Saving Exercise")
                    setMessage("Trouble contacting server, please try again later.")
                    setNegativeButton("Exit") { dialog, _ ->
                        dialog.cancel()
                    }
                    setPositiveButton("Retry") { dialog, _ ->
                        requestEdit()
                    }
                    show()
                }

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }

    // error shown when fields do not pass validation on submit
    private fun invalidEdit() {
        val builder = AlertDialog.Builder(context)
        with(builder)
        {
            setTitle("Error Creating Exercise")
            setMessage("You have entered some fields incorrectly, please try again.")
            setNegativeButton("Retry") { dialog, _ ->
                dialog.cancel()
            }
            show()
        }
    }

    // validity checkers and focus listeners below

    private fun focusListener(container: TextInputLayout, editText: TextView)
    {
        editText.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                container.helperText = validityChecker(editText)
            }
        }
    }

    private fun nameFocusListener(container: TextInputLayout, editText: TextView)
    {
        editText.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                container.helperText = nameValidityChecker(editText)
            }
        }
    }


    private fun validityChecker(editText: TextView): String?
    {
        val hint = editText.hint.toString()
        val text = editText.text.toString()
        if(text.isEmpty())
        {
            return "Please enter a $hint for your exercise."
        }
        if(text.length > 200)
        {
            return "Too many characters."
        }
        return null
    }

    private fun nameValidityChecker(editText: TextView): String?
    {
        val nameText = editText.text.toString()
        if(nameText.isEmpty())
        {
            return "Please enter a name for your exercise."
        }
        if(nameText.length > 50)
        {
            return "Too many characters."
        }
        return null
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CustomExerciseFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            CustomExerciseFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}