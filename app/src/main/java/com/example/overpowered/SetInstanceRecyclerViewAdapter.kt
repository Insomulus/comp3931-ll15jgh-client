package com.example.overpowered

import EditSetFragment
import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import khttp.post
import org.json.JSONException

// process set instances into view recycler
class SetInstanceRecyclerViewAdapter(arrayList: ArrayList<SetInstance>, adapter: ExerciseInstanceRecyclerAdapter) :
    RecyclerView.Adapter<SetInstanceRecyclerViewAdapter.SetInstanceViewHolder>() {
    var setInstanceArrayList: ArrayList<SetInstance> = arrayList
    lateinit var view: View
    lateinit var session: Session
    var adapter = adapter

    // binding required views to variables to be changed later
    class SetInstanceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var setTime: TextView = itemView.findViewById(R.id.timeTxt)
        var setWeight: TextView = itemView.findViewById(R.id.weightTxt)
        var setResistance: TextView = itemView.findViewById(R.id.resistanceTxt)
        var setBodyWeight: TextView = itemView.findViewById(R.id.bodyWeightTxt)
        var setAddedWeight: TextView = itemView.findViewById(R.id.addedWeightTxt)
        var setReps: TextView = itemView.findViewById(R.id.repsTxt)
        var editSetBtn: ImageButton = itemView.findViewById(R.id.setEditBtn)
        var deleteSetBtn: ImageButton = itemView.findViewById(R.id.deleteSetBtn)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SetInstanceViewHolder {
        view = LayoutInflater.from(parent.context)
            .inflate(R.layout.set_item, parent, false)
        session = Session(view.context)
        return SetInstanceViewHolder(view)
    }
    // for current item, bind values to view counterpart then set on click listeners
    override fun onBindViewHolder(holder: SetInstanceViewHolder, position: Int) {
        val currentItem: SetInstance = setInstanceArrayList[position]
        holder.setTime.text = currentItem.time.split(".")[0]
        holder.setWeight.text = currentItem.weight
        holder.setResistance.text = currentItem.resistance
        holder.setBodyWeight.text = currentItem.bodyWeight
        holder.setAddedWeight.text = currentItem.addedWeight
        holder.setReps.text = currentItem.reps
        clickListeners(holder, currentItem)
    }

    // on click listeners for clickable elements within each set
    fun clickListeners(holder: SetInstanceViewHolder, currentItem: SetInstance) {
        // on click of edit button open edit set fragment
        holder.editSetBtn.setOnClickListener {
            val fragment: Fragment = EditSetFragment.newInstance(currentItem, adapter)
            val fragmentManager = (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.replace(R.id.container, fragment, "EDITSET").commit()
        }
        // on click delete button try delete set
        holder.deleteSetBtn.setOnClickListener {
            val builder = AlertDialog.Builder(view.context)
            with(builder)
            // ensure user is sure
            {
                setTitle("Deleting Set")
                setMessage("Are you sure you want to delete this set?")
                setNegativeButton("No") { dialog, _ ->
                    dialog.cancel()
                }
                // if yes contact API end point with set id to delete
                setPositiveButton("Yes") { dialog, _ ->
                    try {
                        val payload = mapOf("set_id" to currentItem.setId)
                        val r = post("http://192.168.0.15:8000/api/delete_set/", data = payload)
                        // if successful, delete
                        if (r.statusCode == 200) {
                            adapter.deleteSet(currentItem)
                            // if unsuccessful, alert user
                        } else {
                            val builder = AlertDialog.Builder(context)
                            with(builder)
                            {
                                setTitle("Error Deleting Set")
                                setMessage("Trouble contacting server, please try again later.")
                                setNegativeButton("Exit") { dialog, _ ->
                                    dialog.cancel()
                                }
                                show()
                            }

                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
                show()
            }
        }

    }



    override fun getItemCount(): Int {
        return setInstanceArrayList.size
    }


}