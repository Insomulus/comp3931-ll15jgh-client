package com.example.overpowered

// exercise item object
class ExerciseItem(
    var exerciseName: String,
    var exerciseDescription: String,
    var exerciseInstructions: String,
    var exerciseImage: String,
    var exerciseType: String,
    var exerciseCategory: List <String>
) {
}
