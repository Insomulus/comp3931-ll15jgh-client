package com.example.overpowered

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import khttp.get
import org.json.JSONArray
import org.json.JSONException

/**
 * A fragment representing a list of Items.
 */

// unused fragment
class ProgressFragment : Fragment() {
    lateinit var session: Session
    lateinit var details: HashMap<String?, String?>
    lateinit var user: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
        }
        session = Session(this.requireContext())
        details = session.getAccountDetails()
        user = details[Session.KEY_USERNAME].toString()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Set the adapter

        return inflater.inflate(R.layout.fragment_progress, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            ProgressFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}