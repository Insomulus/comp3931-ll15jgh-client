package com.example.overpowered

import CreateSetFragment
import android.app.AlertDialog
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import khttp.post
import org.json.JSONException

// process exercise instances into view recycler
class ExerciseInstanceRecyclerAdapter(exerciseList: ArrayList<ExerciseInstance>,
                                      setList: ArrayList<SetInstance>, context: Context) :
    RecyclerView.Adapter<ExerciseInstanceRecyclerAdapter.ExerciseInstanceViewHolder>() {
    private var exerciseInstanceList: ArrayList<ExerciseInstance> = exerciseList
    private var setInstanceList: ArrayList<SetInstance> = setList
    private var recyclerContext: Context = context
    lateinit var session: Session


    // binding required views to variables to be changed later
    class ExerciseInstanceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var exerciseName: TextView = itemView.findViewById(R.id.exerciseInstanceTitle)
        var instanceRecyclerView: RecyclerView = itemView.findViewById(R.id.exerciseRecyclerView)
        var exerciseAddSetBtn: ImageButton = itemView.findViewById(R.id.exerciseInstanceAdd)
        var exerciseDeleteInstanceBtn: ImageButton = itemView.findViewById(R.id.exerciseInstanceDelete)
        var expandSetsBtn: ImageButton = itemView.findViewById(R.id.exerciseInstanceExpand)
        var totalSets: TextView = itemView.findViewById(R.id.instanceSetsTxt2)
        var totalReps: TextView = itemView.findViewById(R.id.instanceRepsTxt2)
        var totalVolume: TextView = itemView.findViewById(R.id.instanceVolume2)

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExerciseInstanceViewHolder {
        val view: View = LayoutInflater.from(parent.context)
            .inflate(R.layout.exercise_instance_item, parent, false)
        session = Session(view.context)
        return ExerciseInstanceViewHolder(view)
    }

    // unused now
    override fun getItemCount(): Int {
        return exerciseInstanceList.size
    }

    // on bind get exercise from list, and bind data, if it matches ID of set in set list, create
    // new list and add set to list, when total number of sets matching exercises is found, pass
    // to set adapter and repeat for next item exercise in position
    override fun onBindViewHolder(holder: ExerciseInstanceViewHolder, position: Int) {
        val currentItem: ExerciseInstance = exerciseInstanceList[position]
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(recyclerContext, LinearLayoutManager.VERTICAL, false)
        holder.instanceRecyclerView.layoutManager = layoutManager
        holder.instanceRecyclerView.setHasFixedSize(true)
        holder.exerciseName.text = currentItem.exercise
        // sets on click listeners for views in current exercise instance item
        clickListeners(holder, currentItem)
        // new list for sets that should be with this exercise instance
        val arrayList: ArrayList<SetInstance> = ArrayList()
        Log.d("PRINTING list of item ", setInstanceList.toString())
        var setCount = 0
        var repCount = 0
        var volCount = 0
        // for each element in set instance list
        for (i in 0 until setInstanceList.size) {
            // compare ID to exercise instance ID, if match then add to list
                if(exerciseInstanceList[position].instanceId == setInstanceList[i].instanceId) {
                    Log.d("PRINTING category of item ", setInstanceList[i].instanceId)
                    arrayList.add(setInstanceList[i])
                    // pull stats from set and add to count
                    setCount += 1
                    repCount += setInstanceList[i].reps.toInt()
                    volCount += setInstanceList[i].weight.toInt()  * setInstanceList[i].reps.toInt()
                }
            }
        // pass exercise instance set totals to views
        holder.totalSets.text = setCount.toString()
        holder.totalReps.text = repCount.toString()
        holder.totalVolume.text = volCount.toString()

        Log.d("PRINTING array ", arrayList.toString())
        // move to set adapter with new list
        val exerciseInstanceRecyclerViewAdapter =
            SetInstanceRecyclerViewAdapter(arrayList, this)
        holder.instanceRecyclerView.adapter = (exerciseInstanceRecyclerViewAdapter)



    }
    // on click listeners to bind responses to each clickable aspect of the exercise instance box
    // for the current instance being processed
    fun clickListeners(holder: ExerciseInstanceViewHolder, currentItem: ExerciseInstance ) {
        // if expand is clicked, hide or make visible all children sets
        holder.expandSetsBtn.setOnClickListener {
            if(holder.instanceRecyclerView.visibility == View.GONE) {
                holder.instanceRecyclerView.visibility = View.VISIBLE
            } else {
                holder.instanceRecyclerView.visibility = View.GONE
            }
        }
        // if add is clicked open create set fragment
        holder.exerciseAddSetBtn.setOnClickListener {
            Log.d("PRINTING SETLIST ", holder.exerciseName.text.toString())
            val fragment: Fragment =
                CreateSetFragment.newInstance(currentItem.instanceId, currentItem.exercise, this)
            val fragmentManager =
                (recyclerContext as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.replace(R.id.container, fragment, "CREATESET").commit()

        }
        // if delete is clicked then delete set
        holder.exerciseDeleteInstanceBtn.setOnClickListener {
            Log.d("PRINTING SETLIST ", holder.exerciseName.text.toString())
            // confirm that the user is sure
            val builder = AlertDialog.Builder(recyclerContext)
            with(builder)
            {
                setTitle("Deleting Exercise")
                setMessage("Are you sure you want to delete exercise and all its sets?")
                setNegativeButton("No") { dialog, _ ->
                    dialog.cancel()
                }
                // if sure, try to pass id to API end point
                setPositiveButton("Yes") { dialog, _ ->
                    try {
                        val payload = mapOf("instance_id" to currentItem.instanceId)
                        val r = post(
                            "http://192.168.0.15:8000/api/delete_exercise_instance/",
                            data = payload
                        )
                        // if successful, current item is deleted from database, delete from local
                        // list also
                        if (r.statusCode == 200) {
                            println("ok")
                            deleteInstance(currentItem)
                            // if there are no instances left, load diary no exercise fragment
                            if (exerciseInstanceList.isEmpty()) {
                                val fragment: Fragment = DiaryNoExercise()
                                val fragmentManager = (context as FragmentActivity).supportFragmentManager.beginTransaction()
                                fragmentManager.replace(R.id.contentLayout, fragment, "NOEXERCISE").commit()
                            }
                        } else {
                            // if there was an issue deleting, present dialog
                            val builder = AlertDialog.Builder(context)
                            with(builder)
                            {
                                setTitle("Error Deleting Set")
                                setMessage("Trouble contacting server, please try again later.")
                                setNegativeButton("Exit") { dialog, _ ->
                                    dialog.cancel()
                                }
                                show()
                            }

                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                }
                show()
            }

        }
    }

    // removes current exercise instance and alerts adapter
    fun deleteInstance(currentItem: ExerciseInstance) {
        exerciseInstanceList.remove(currentItem) // THIS HERE!!!
        exerciseInstanceRecyclerView.adapter?.notifyDataSetChanged()
    }
    // adds current exercise instance and alerts adapter
    fun addInstance(currentItem: ExerciseInstance) {
        exerciseInstanceList.add(currentItem) // THIS HERE!!!
        exerciseInstanceRecyclerView.adapter?.notifyDataSetChanged()
    }
    // removes current set instance and alerts adapter
    fun deleteSet(currentItem: SetInstance) {
        setInstanceList.remove(currentItem) // THIS HERE!!!
        exerciseInstanceRecyclerView.adapter?.notifyDataSetChanged()
    }
    // adds current set instance and alerts adapter
    fun addSet(currentItem: SetInstance) {
        setInstanceList.add(currentItem) // THIS HERE!!!
        exerciseInstanceRecyclerView.adapter?.notifyDataSetChanged()
        Log.d("beans", "saving set")
    }
    }





