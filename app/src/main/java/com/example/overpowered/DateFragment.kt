package com.example.overpowered

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProvider
import com.example.overpowered.Session.Companion.KEY_USERNAME
import kotlinx.android.synthetic.main.fragment_date.*
import kotlinx.android.synthetic.main.fragment_date.view.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [DateFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

// the Date Fragment that handles the small navigation pane at the top of the application
// changes the date depending on user selection
class DateFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var formattedLocalDate: String
    lateinit var session: Session
    lateinit var details: HashMap<String?, String?>
    lateinit var user: String

    @RequiresApi(Build.VERSION_CODES.O)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        session = Session(this.requireContext())
        details = session.getAccountDetails()
        user = details[KEY_USERNAME].toString()
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)

        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_date, container, false)
        // format the date
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
        // grabs the current local time
        val rawDate = LocalDate.now()
        formattedLocalDate = rawDate.format(formatter)
        // change the displayed date to the formatted local date on create
        view.dateDisplay.text = formattedLocalDate
        // store the new date within the session
        session.changeDate(formattedLocalDate)
        print(view.dateDisplay.text)
        // removed depreciated code
        //val user = preferences.getString(KEY_USERNAME, "username").toString()
        //(activity as MainActivity).showSets(user, formattedLocalDate )
        return view

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // creates a shared data view model that listens for changes made to the date via the calendar
        // or via clicks in this fragment, refresh teh diary fragment below whenever the date is changed
        val model = ViewModelProvider(requireActivity())[DateSharedViewModel::class.java]
        model.getStored().observe(
            viewLifecycleOwner
        ) {
            formattedLocalDate = model.getStored().value.toString()
            dateDisplay.text = formattedLocalDate
            session.changeDate(formattedLocalDate)
            val fragmentManager = (context as FragmentActivity).supportFragmentManager.beginTransaction()
            (context as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.replace(R.id.contentLayout, DiaryFragment(), "DIARY").commit()
        }
        view.rightButton.setOnClickListener {
            increaseDate(formattedLocalDate)
            session.changeDate(formattedLocalDate)
            val fragmentManager = (context as FragmentActivity).supportFragmentManager.beginTransaction()
            (context as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.replace(R.id.contentLayout, DiaryFragment(), "DIARY").commit()
        }
        view.leftButton.setOnClickListener {
            decreaseDate(formattedLocalDate)
            session.changeDate(formattedLocalDate)
            val fragmentManager = (context as FragmentActivity).supportFragmentManager.beginTransaction()
            (context as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.replace(R.id.contentLayout, DiaryFragment(), "DIARY").commit()
        }
        // launches the calendar
        view.downButton.setOnClickListener {
            (activity as MainActivity).showCalendarDialog(view)
            session.changeDate(formattedLocalDate)
        }
    }
    // calculates the new date when the user clicks the increase date button, fully accounts for
    // leap years, updates the date in the session and returns the new date
    private var days: MutableList<Int> = mutableListOf(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31)
    private fun increaseDate(date : String): String {
        val parsed = date.split("-").toTypedArray()
        var day = parsed[0].toInt()
        var month = parsed[1].toInt()
        var year = parsed[2].toInt()

        Log.d("SUCCESS","$day-$month-$year")
        // leap year calculations
        if (year % 4 == 0){
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    days[1] = 29
                }
                else {
                    days[1] = 28
                }
            }
            else {
                days[1] = 29
            }

        }
        else{
            days[1] = 28
        }

        if (day >= days[month-1]) {
            day = 1
            month += 1
            if (month > 12)  {
                year += 1
                month = 1
            }
        }
        else if (day < days[month-1]) {
            day += 1
        }
        var adjustedDay = day.toString()
        var adjustedMonth = month.toString()

        if (day < 10) {
            adjustedDay = "0$adjustedDay"
        }
        if (month < 10) {
            adjustedMonth = "0$adjustedMonth"
        }
        formattedLocalDate = "$adjustedDay-$adjustedMonth-$year"
        dateDisplay.text = formattedLocalDate
        session.changeDate(formattedLocalDate)
        return formattedLocalDate
    }


    // calculates the new date when the user clicks the decrease date button, fully accounts for
    // leap years, updates the date in the session and returns the new date
    private fun decreaseDate(date : String): String {
        val parsed = date.split("-").toTypedArray()
        var day = parsed[0].toInt()
        var month = parsed[1].toInt()
        var year = parsed[2].toInt()

        if (year % 4 == 0){
            if (year % 100 == 0) {
                if (year % 400 == 0) {
                    days[1] = 29
                }
                else {
                    days[1] = 28
                }
            }
            else {
                days[1] = 29
            }

        }
        else{
            days[1] = 28
        }

        if (day <= 1) {
            Log.d("check month", month.toString())
            if (month > 1)  {
                month += -1
                day = days[month - 1]
                Log.d("month decrease", month.toString())
            }
            else {
                year += -1
                month = 12
                day = days[month-1]
                Log.d("check month2", month.toString())
                Log.d("year decrease", year.toString())
            }
        }
        else if (day <= days[month-1]) {
            day += -1
            Log.d("date decrease", day.toString())
        }
        Log.d("it is this time", "$day-$month-$year")

        var adjustedDay = day.toString()
        var adjustedMonth = month.toString()

        if (day < 10) {
            adjustedDay = "0$adjustedDay"
        }
        if (month < 10) {
            adjustedMonth = "0$adjustedMonth"
        }
        formattedLocalDate = "$adjustedDay-$adjustedMonth-$year"
        Log.d("SUCCESS", formattedLocalDate)
        dateDisplay.text = formattedLocalDate
        session.changeDate(formattedLocalDate)
        return formattedLocalDate
    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment DateFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            DateFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}