package com.example.overpowered

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

// shares date from calendar to date fragment via view model
class DateSharedViewModel : ViewModel() {
    private val storedString = MutableLiveData<String>()
    fun store(string: String) {
        storedString.value = string
    }

    fun getStored(): MutableLiveData<String> {
        return storedString
    }
}