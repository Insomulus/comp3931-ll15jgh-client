import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.overpowered.ExerciseInstanceRecyclerAdapter
import com.example.overpowered.R
import com.example.overpowered.Session
import com.example.overpowered.SetInstance
import com.google.android.material.textfield.TextInputLayout
import khttp.post
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.fragment_edit_set.*
import kotlinx.android.synthetic.main.fragment_edit_set.view.*
import org.json.JSONException
import java.time.LocalTime


import java.util.*
import kotlin.collections.HashMap


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_PARAM3 = "param3"
private const val ARG_PARAM4 = "param4"
private const val ARG_PARAM5 = "param5"
private const val ARG_PARAM6 = "param6"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment2.newInstance] factory method to
 * create an instance of this fragment.
 */
class CreateSetFragment: Fragment() {
    private var _binding: CreateSetFragment? = null
    private val binding get() = _binding!!
    var fragmentContext: Fragment = this
    lateinit var session: Session
    lateinit var details: HashMap<String?, String?>
    lateinit var user: String
    private lateinit var adapter2: ExerciseInstanceRecyclerAdapter

    companion object {
        const val ARG_INSTANCE = "INSTANCE ID"
        const val ARG_EXERCISE_NAME = "EXERCISE_NAME"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment2.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param7: String, adapter: ExerciseInstanceRecyclerAdapter) =
            CreateSetFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_INSTANCE, param1)
                    putString(ARG_EXERCISE_NAME, param7)
                    adapter2 = adapter


                }
            }
    }
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var param3: String? = null
    private var param4: String? = null
    private var param5: String? = null
    private var param6: String? = null
    private var param7: String? = null
    private lateinit var rawTime: LocalTime

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        session = Session(this.requireContext())
        details = session.getAccountDetails()
        user = details[Session.KEY_USERNAME].toString()
        arguments?.let {
            param1 = it.getString(ARG_INSTANCE)
            param7 = it.getString(ARG_EXERCISE_NAME)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_set, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view.findViewById<TextView>(R.id.excerciseTxt).text = param7
        Log.d("TITLE ", param7.toString())
        val backButton: Button = view.findViewById(R.id.backButtonExercise)
        backButton.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
            Log.d("CLICK CHECKER", "OK")
        }


        view.findViewById<Button>(R.id.editBtnEx).setOnClickListener{
            saveEdit()
        }
        focusListener(view.weightContainer, view.weightEditText)
        focusListener(view.resistanceContainer, view.resistanceEditText)
        focusListener(view.bodyWeightContainer, view.bodyWeightEditText)
        focusListener(view.addedWeightContainer, view.addedWeightEditText)
        focusListener(view.repsContainer, view.repsEditText)

    }


    fun saveEdit() {
        rawTime = LocalTime.now()
        val weightContainer = view?.findViewById<TextInputLayout>(R.id.weightContainer)
        param2 = weightContainer?.weightEditText?.text.toString()
        weightContainer?.helperText = weightContainer?.let { validityChecker(it.weightEditText) }
        val validWeight = weightContainer?.helperText == null
        val resistanceContainer = view?.findViewById<TextInputLayout>(R.id.resistanceContainer)
        param3 = resistanceContainer?.resistanceEditText?.text.toString()
        resistanceContainer?.helperText = resistanceContainer?.let { validityChecker(it.resistanceEditText) }
        val validResistance = resistanceContainer?.helperText == null
        val bodyWeightContainer = view?.findViewById<TextInputLayout>(R.id.bodyWeightContainer)
        param4 = bodyWeightContainer?.bodyWeightEditText?.text.toString()
        bodyWeightContainer?.helperText = bodyWeightContainer?.let { validityChecker(it.bodyWeightEditText) }
        val validBodyWeight = bodyWeightContainer?.helperText == null
        val addedWeightContainer = view?.findViewById<TextInputLayout>(R.id.addedWeightContainer)
        param5 = addedWeightContainer?.addedWeightEditText?.text.toString()
        addedWeightContainer?.helperText = addedWeightContainer?.let { validityChecker(it.addedWeightEditText) }
        val validAddedWeight = addedWeightContainer?.helperText == null
        val repsContainer = view?.findViewById<TextInputLayout>(R.id.repsContainer)
        param6 = repsContainer?.repsEditText?.text.toString()
        repsContainer?.helperText = repsContainer?.let { validityChecker(it.repsEditText) }
        val validReps = repsContainer?.helperText == null

        if (validWeight && validResistance && validBodyWeight
            && validAddedWeight && validReps) {
            confirmCreate()
        }
        else {
            invalidEdit()
        }

    }


    private fun confirmCreate() {
        val builder = AlertDialog.Builder(context)
        with(builder)
        {
            setTitle("Creating Set")
            setMessage("Are you sure you want to create this set and return to diary?")
            setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }
            setPositiveButton("Yes") { dialog, _ ->
                requestCreate()
            }
            show()
        }
    }

    private fun requestCreate() {
        try {
            val payload = mapOf("username" to user, "weight" to param2,
                "resistance" to param3, "body_weight" to param4, "added_weight" to param5,
                "reps" to param6, "instance" to param1, "exercise" to param7)
            val r = post("http://192.168.0.15:8000/api/create_set/", data = payload)
            if (r.statusCode == 200) {
                adapter2.addSet(SetInstance(rawTime.toString(), param2.toString(), param3.toString(),
                    param4.toString(), param5.toString(), param6.toString(), param1.toString(),
                    ""))
                activity?.supportFragmentManager?.beginTransaction()
                    ?.remove(fragmentContext)
                    ?.commit()
            } else {
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error Creating Set")
                    setMessage("Trouble contacting server, please try again later.")
                    setNegativeButton("Exit") { dialog, _ ->
                        dialog.cancel()
                    }
                    setPositiveButton("Retry") { dialog, _ ->
                        requestCreate()
                    }
                    show()
                }

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
    private fun invalidEdit() {
        val builder = AlertDialog.Builder(context)
        with(builder)
        {
            setTitle("Error Creating Set")
            setMessage("You have entered some incorrect values, please try again.")
            setNegativeButton("Retry") { dialog, _ ->
                dialog.cancel()
            }
            show()
        }
    }

    private fun focusListener(container: TextInputLayout, editText: TextView)
    {
        editText.setOnFocusChangeListener { _, focused ->
            if(!focused) {
                container.helperText = validityChecker(editText)
            }
        }
    }


    private fun validityChecker(editText: TextView): String?
    {
        val weightText = editText.text.toString()
        if(weightText.isEmpty())
        {
            return "Please enter a value."
        }
        else if(!weightText.matches(".*[0-9].*".toRegex()))
        {
            return "Please only enter digits."
        }
        if(weightText.length > 4)
        {
            return "Cannot be more than 4 digits, you're not that strong..."
        }
        return null
    }


}