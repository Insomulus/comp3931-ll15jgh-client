import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.overpowered.ExerciseInstanceRecyclerAdapter
import com.example.overpowered.R
import com.example.overpowered.Session
import com.example.overpowered.SetInstance
import com.google.android.material.textfield.TextInputLayout
import khttp.post
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.fragment_edit_set.*
import kotlinx.android.synthetic.main.fragment_edit_set.view.*
import org.json.JSONException


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
private const val ARG_PARAM3 = "param3"
private const val ARG_PARAM4 = "param4"
private const val ARG_PARAM5 = "param5"
private const val ARG_PARAM6 = "param6"

/**
 * A simple [Fragment] subclass.
 * Use the [Fragment2.newInstance] factory method to
 * create an instance of this fragment.
 */

// edit set fragment allows user to edit selected set
class EditSetFragment: Fragment() {
    lateinit var session: Session
    lateinit var details: HashMap<String?, String?>
    lateinit var user: String

    companion object {
        const val ARG_INSTANCE = "INSTANCE ID"
        const val ARG_WEIGHT = "WEIGHT"
        const val ARG_RESISTANCE = "RESISTANCE"
        const val ARG_BODY_WEIGHT = "BODY WEIGHT"
        const val ARG_ADDED_WEIGHT = "ADDED WEIGHT"
        const val ARG_REPS = "REPS"
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment Fragment2.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(set: SetInstance, adapter: ExerciseInstanceRecyclerAdapter) =
            EditSetFragment().apply {
                arguments = Bundle().apply {
                    // set variables
                    adapter2 = adapter
                    currentItem = set
                    time = currentItem.time
                    weight = currentItem.weight
                    resistance = currentItem.resistance
                    bodyWeight = currentItem.bodyWeight
                    addedWeight = currentItem.addedWeight
                    reps = currentItem.reps
                    instanceId = currentItem.instanceId
                    setId = currentItem.setId

                }
            }
    }
    // variables to be used in class
    private var time: String? = null
    private var weight: String? = null
    private var resistance: String? = null
    private var bodyWeight: String? = null
    private var addedWeight: String? = null
    private var reps: String? = null
    private var instanceId: String? = null
    private var setId: String? = null
    private lateinit var adapter2: ExerciseInstanceRecyclerAdapter
    private lateinit var currentItem: SetInstance

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        session = Session(this.requireContext())
        details = session.getAccountDetails()
        user = details[Session.KEY_USERNAME].toString()
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_set, container, false)
    }

    // populates the edit set form with the current data for the selected set
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.findViewById<TextView>(R.id.excerciseTxt).text = setId
        view.findViewById<TextView>(R.id.weightEditText).text = weight
        view.findViewById<TextView>(R.id.resistanceEditText).text = resistance
        view.findViewById<TextView>(R.id.bodyWeightEditText).text = bodyWeight
        view.findViewById<TextView>(R.id.addedWeightEditText).text = addedWeight
        view.findViewById<TextView>(R.id.repsEditText).text = reps

        // on click back button return to previous fragment
        val backButton: Button = view.findViewById(R.id.backButtonExercise)
        backButton.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
            Log.d("CLICK CHECKER", "OK")
        }

        // on click save edit try save edit
        view.findViewById<Button>(R.id.editBtnEx).setOnClickListener{
            saveEdit()
        }

        // set focus listener on each text box to perform actions on lose/gain focus
        focusListener(view.weightContainer, view.weightEditText)
        focusListener(view.resistanceContainer, view.resistanceEditText)
        focusListener(view.bodyWeightContainer, view.bodyWeightEditText)
        focusListener(view.addedWeightContainer, view.addedWeightEditText)
        focusListener(view.repsContainer, view.repsEditText)

    }

    // on save check validity of user input, if all valid then submit edit
    fun saveEdit() {
        val weightContainer = view?.findViewById<TextInputLayout>(R.id.weightContainer)
        weight = weightContainer?.weightEditText?.text.toString()
        weightContainer?.helperText = weightContainer?.let { validityChecker(it.weightEditText) }
        val validWeight = weightContainer?.helperText == null
        val resistanceContainer = view?.findViewById<TextInputLayout>(R.id.resistanceContainer)
        resistance = resistanceContainer?.resistanceEditText?.text.toString()
        resistanceContainer?.helperText = resistanceContainer?.let { validityChecker(it.resistanceEditText) }
        val validResistance = resistanceContainer?.helperText == null
        val bodyWeightContainer = view?.findViewById<TextInputLayout>(R.id.bodyWeightContainer)
        bodyWeight = bodyWeightContainer?.bodyWeightEditText?.text.toString()
        bodyWeightContainer?.helperText = bodyWeightContainer?.let { validityChecker(it.bodyWeightEditText) }
        val validBodyWeight = bodyWeightContainer?.helperText == null
        val addedWeightContainer = view?.findViewById<TextInputLayout>(R.id.addedWeightContainer)
        addedWeight = addedWeightContainer?.addedWeightEditText?.text.toString()
        addedWeightContainer?.helperText = addedWeightContainer?.let { validityChecker(it.addedWeightEditText) }
        val validAddedWeight = addedWeightContainer?.helperText == null
        val repsContainer = view?.findViewById<TextInputLayout>(R.id.repsContainer)
        reps = repsContainer?.repsEditText?.text.toString()
        repsContainer?.helperText = repsContainer?.let { validityChecker(it.repsEditText) }
        val validReps = repsContainer?.helperText == null

        if (validWeight && validResistance && validBodyWeight
            && validAddedWeight && validReps) {
            submitEdit()
        }
        else {
            invalidEdit()
        }

    }

    // confirm the user wishes to submit changes
    private fun submitEdit() {
        val builder = AlertDialog.Builder(context)
        with(builder)
        {
            setTitle("Saving Changes")
            setMessage("Are you sure you want to save these changes and return to your diary?")
            setNegativeButton("No") { dialog, _ ->
                dialog.cancel()
            }
            setPositiveButton("Yes") { dialog, _ ->
                requestEdit()
            }
            show()
        }
    }
    // contact API end point to request set edit
    private fun requestEdit() {
        try {
            val payload = mapOf("username" to user, "set_id" to setId, "weight" to weight,
                "resistance" to resistance, "body_weight" to bodyWeight, "added_weight" to addedWeight,
                "reps" to reps)
            val r = post("http://192.168.0.15:8000/api/edit_set/", data = payload)
            // if successful, delete current item from adapter and re-add it with new values
            if (r.statusCode == 200) {
                adapter2.deleteSet(currentItem)
                adapter2.addSet(SetInstance(time.toString(), weight.toString(), resistance.toString(),
                bodyWeight.toString(), addedWeight.toString(), reps.toString(), instanceId.toString(),
                setId.toString()))
                // remove current fragment and return
                activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
            } else {
                // if fails, show error dialog
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error Saving Changes")
                    setMessage("Trouble contacting server, please try again later.")
                    setNegativeButton("Exit") { dialog, _ ->
                        dialog.cancel()
                    }
                    setPositiveButton("Retry") { dialog, _ ->
                        requestEdit()
                    }
                    show()
                }

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }
    // if validation fails and user attempts submit, inform of error
    private fun invalidEdit() {
        val builder = AlertDialog.Builder(context)
        with(builder)
        {
            setTitle("Error Saving Changes")
            setMessage("You have entered some incorrect values, please try again.")
            setNegativeButton("Retry") { dialog, _ ->
                dialog.cancel()
            }
            show()
        }
    }

    // focus listener calls validity check when the focus is moved away from current text box
    private fun focusListener(container: TextInputLayout, editText: TextView)
    {
        editText.setOnFocusChangeListener { _, focused ->
                if(!focused) {
                    container.helperText = validityChecker(editText)
                }
            }
    }

    // makes sure forms are not empty, that they are digits and they are no more than 4 in length
    private fun validityChecker(editText: TextView): String?
    {
        val weightText = editText.text.toString()
        if(weightText.isEmpty())
        {
            return "Please enter a value."
        }
        else if(!weightText.matches(".*[0-9].*".toRegex()))
        {
            return "Please only enter digits."
        }
        if(weightText.length > 4)
        {
            return "Cannot be more than 4 digits, you're not that strong..."
        }
        return null
    }


}