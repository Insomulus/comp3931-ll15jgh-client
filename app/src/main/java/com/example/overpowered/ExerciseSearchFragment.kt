package com.example.overpowered

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import khttp.get
import kotlinx.android.synthetic.main.fragment_exercise_search.*
import org.json.JSONArray
import org.json.JSONException


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ExerciseSearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

// exercise search fragment for search page where user can find exercises
class ExerciseSearchFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    lateinit var categoryRecyclerView: RecyclerView
    private lateinit var categoryAdapter: RecyclerView.Adapter<*>
    var categoryArrayList: ArrayList<ExerciseCategory> = ArrayList()
    var exerciseArrayList: ArrayList<ExerciseItem> = ArrayList()
    private lateinit var categoryLayout: RecyclerView.LayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exercise_search, container, false)
    }

    // on create, pull list of exercises from server and process them via adapters
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pullExercises()
        // once exercises are pulled, push data to adapters
        categoryRecyclerView = view.findViewById(R.id.exerciseCategoryRecyclerView)
        categoryRecyclerView.setHasFixedSize(true)
        categoryLayout = LinearLayoutManager(context)
        categoryAdapter = ExerciseCategoryRecyclerAdapter(categoryArrayList, exerciseArrayList, requireContext())
        categoryRecyclerView.layoutManager = categoryLayout
        categoryRecyclerView.adapter = categoryAdapter
        categoryAdapter.notifyDataSetChanged()

        // on click listener for create custom exercise button, on click launch custom exercise fragment
        createExerciseBtn.setOnClickListener{
            val fragment: Fragment = CustomExerciseFragment()
            val fragmentManager = (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.add(R.id.contentLayout, fragment, "CUSTOM").commit()
        }
        // on click listener for back button returns to previous fragment
        backbtnSearch.setOnClickListener {
            val fragment: Fragment = DiaryFragment()
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
            val fragmentManager = (view.context as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.add(R.id.contentLayout, fragment, "DIARY").commit()
        }
    }

    // pull exercises from server
    private fun pullExercises() {
        // try pull exercises from API end point
        try {
            val r = get("http://192.168.0.15:8000/api/pull_exercises/")
            // if successful then split into categories and exercises then process
            if (r.statusCode == 200) {
                val categories = r.jsonObject.getJSONArray("categories")
                val exercises = r.jsonObject.getJSONArray("exercises")
                processExercises(categories, exercises)
            } else {
                // if unsuccessful notify user
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error Pulling Exercises")
                    setMessage("Trouble contacting server, please try again later.")
                    setNegativeButton("Exit") { dialog, _ ->
                        dialog.cancel()
                    }
                    setPositiveButton("Retry") { dialog, _ ->
                        pullExercises()
                    }
                    show()
                }

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    // process the two JSONArrays into array lists containing exercise categories and items
    fun processExercises(categories: JSONArray, exercises: JSONArray) {
        for (i in 0 until categories.length()) {
            val currentCategory = categories[i].toString()
            // add exercise category to list
            categoryArrayList.add(ExerciseCategory(currentCategory))
        }
        for (i in 0 until exercises.length()) {
            var exerciseCategoryList: List<String> = emptyList()
            val currentExercise = exercises.getJSONObject(i)
            val exerciseName = currentExercise.get("name").toString()
            val exerciseDescription = currentExercise.get("description").toString()
            val exerciseInstructions = currentExercise.get("instructions").toString()
            val exerciseImage = currentExercise.get("image").toString()
            val exerciseType = currentExercise.get("type").toString()
            val exerciseCategory = currentExercise.getJSONArray("category")
            for (i in 0 until exerciseCategory.length())
                exerciseCategoryList = exerciseCategoryList + (exerciseCategory.getJSONObject(i).get("category").toString())
            // add exercise to list
            exerciseArrayList.add(ExerciseItem(exerciseName, exerciseDescription, exerciseInstructions,
            exerciseImage, exerciseType, exerciseCategoryList))

        }


    }



    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ExerciseSearchFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ExerciseSearchFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}