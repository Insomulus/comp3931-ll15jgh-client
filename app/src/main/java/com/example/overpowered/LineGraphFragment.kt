package com.example.overpowered

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import khttp.get
import kotlinx.android.synthetic.main.fragment_bar_chart.*
import org.json.JSONException
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [LineGraphFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

// line graph fragment displays a line graph for selected exercise based on user progress
class LineGraphFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var exerciseName: String? = null
    lateinit var session: Session
    lateinit var details: HashMap<String?, String?>
    private var user: String? = null

    private var setList = ArrayList<Set>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        session = Session(this.requireContext())
        details = session.getAccountDetails()
        user = details[Session.KEY_USERNAME].toString()
        exerciseName = param1
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bar_chart, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // attempt to populate the set list for exercise
        setList = getSetData()
        // if there are no sets warn user
        if (setList.isEmpty()) {
            setTitleTxt.text = "No sets for this exercises recorded"
        }
        else {
            // if there are sets then render graph
            var boolChange = true
            // apply graph settings
            graphSettings()
            // add data to graph
            addSetData(boolChange)
            // sets the max number of values visible without scrolling in graph
            lineChart.setVisibleXRangeMaximum(8f)
            lineChart.setVisibleXRangeMinimum(4f)
            // on click to change type of data shown in graph
            chartToggle.setOnClickListener {
                // change bool each click
                boolChange = !boolChange
                addSetData(boolChange)
                chartToggle.text = "Toggle To Weight"
            }
        }

        // on click to return to previous fragment
        chartBack.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
            val fragment: Fragment = ExerciseSearchFragment()
            val fragmentManager =
                (context as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.replace(R.id.contentLayout, fragment, "SEARCH").commit()
        }

    }

    // applies custom settings to visual element of graph
    private fun graphSettings() {

        val xAxis: XAxis = lineChart.xAxis
        val yAxis: YAxis = lineChart.axisLeft
        lineChart.description.isEnabled = false
        xAxis.setDrawGridLines(false)
        xAxis.setDrawLabels(true)
        xAxis.granularity = 1F
        xAxis.valueFormatter = AxisFormat()
        xAxis.textColor = resources.getColor(R.color.colorWhite)
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        yAxis.setDrawGridLines(false)
        yAxis.setDrawLabels(true)
        yAxis.textColor = resources.getColor(R.color.colorWhite)
        lineChart.isHighlightPerTapEnabled = true
        lineChart.axisRight.isEnabled = false
        lineChart.extraRightOffset = 30f
        lineChart.setNoDataTextColor(R.color.colorWhite)
        lineChart.animateX(600, Easing.EaseInBounce)


    }

    inner class AxisFormat : IndexAxisValueFormatter() {

        override fun getAxisLabel(value: Float, axis: AxisBase?): String {
            val index = value.toInt()
            return if (index < setList.size) {
                setList[index].time
            } else {
                ""
            }
        }
    }

    // add data to graph
    private fun addSetData(type: Boolean) {
        val entries: ArrayList<Entry> = ArrayList()
        // if bool == true then add set weight data
        if (type) {
            for (i in setList.indices) {
                val set = setList[i]
                entries.add(Entry(i.toFloat(), set.weight.toFloat()))
            }
        }
        // else bool == false then add set reps data
        else {
            for (i in setList.indices) {
                val set = setList[i]
                entries.add(Entry(i.toFloat(), set.reps.toFloat()))
            }

        }

        // apply visual customisation to data
        val dataSet = LineDataSet(entries, "" )
        dataSet.valueTextColor = R.color.colorWhite
        dataSet.lineWidth = 2f
        val legend = lineChart.legend
        legend.isEnabled = false
        val data = LineData(dataSet)
        // add data to graph
        lineChart.data = data
        lineChart.invalidate()
    }

    // get data from server
    private fun getSetData(): ArrayList<Set> {
        try {
            // try to to get set data for selected exercise belonging to current user from API end point
            val payload = mapOf("username" to user, "exercise" to exerciseName)
            val r = get("http://192.168.0.15:8000/api/pull_set_graph_data/", data = payload)
            // if successful parse array and add Set items to set list
            if (r.statusCode == 200) {
                val sets = r.jsonObject.getJSONArray("sets")
                for (i in 0 until sets.length()) {
                    val currentSet = sets.getJSONObject(i)
                    val setTime = currentSet.get("time").toString().split('.')
                    val setWeight = currentSet.get("weight").toString()
                    val setReps = currentSet.get("reps").toString()
                    setList.add(Set(setTime[0], setWeight.toInt(), setReps.toInt()))
                }
                // if unsuccessful, do nothing
            } else if (r.statusCode == 404){
                Log.d("404", "NO DATA")
            }
            // warn user there was an issue
            else
            {
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error Pulling Exercises")
                    setMessage("Trouble contacting server, please try again later.")
                    setNegativeButton("Exit") { dialog, _ ->
                        dialog.cancel()
                    }
                    setPositiveButton("Retry") { dialog, _ ->
                        dialog.cancel()
                    }
                    show()
                }

            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }




        return setList
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BarChartFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String) =
            LineGraphFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                }
            }
    }
}

