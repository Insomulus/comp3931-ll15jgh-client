package com.example.overpowered

import android.app.AlertDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import khttp.post
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.exercise__search_category.*
import kotlinx.android.synthetic.main.fragment_exercise_information.*
import org.json.JSONException

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"

/**
 * A simple [Fragment] subclass.
 * Use the [ExerciseInformationFragment.newInstance] factory method to
 * create an instance of this fragment.
 */

// exercise information fragment to display information about selected exercise
class ExerciseInformationFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: Bitmap? = null
    private var param3: String? = null
    private var param4: String? = null
    private var param5: String? = null
    lateinit var session: Session
    lateinit var details: HashMap<String?, String?>
    lateinit var user: String
    lateinit var sessionDate: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        session = Session(this.requireContext())
        details = session.getAccountDetails()
        user = details[Session.KEY_USERNAME].toString()
        sessionDate = details[Session.KEY_DATE].toString()
        arguments?.let {

        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_exercise_information, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        // set views to variables passed when creating instance
        view.findViewById<TextView>(R.id.excerciseTxt).text = param1
        view.findViewById<ImageView>(R.id.createExerciseImg).setImageBitmap(param2)
        view.findViewById<TextView>(R.id.exerciseDescriptionTxt2).text = param3
        view.findViewById<TextView>(R.id.exerciseInstructionsTxt2).text = param4
        view.findViewById<TextView>(R.id.exerciseTypeTxt2).text = param5

        // on click back button return to previous fragment
        backButtonExercise.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
            val fragment: Fragment = ExerciseSearchFragment()
            val fragmentManager =
                (context as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.replace(R.id.contentLayout, fragment, "SEARCH").commit()
            Log.d("CLICK CHECKER", sessionDate)
        }
        // on click edit button attempt to add exercise
        editBtnEx.setOnClickListener {
            val builder = AlertDialog.Builder(context)
            with(builder)
            {
                setTitle("Add Exercise?")
                setMessage("Create an exercise instance for date: $sessionDate?")
                setNegativeButton("Cancel") { dialog, _ ->
                    dialog.cancel()
                }
                setPositiveButton("Yes") { dialog, _ ->
                    addExerciseInstance()
                }
                show()
            }
            Log.d("CLICK CHECKER", sessionDate)
        }

        // on click graph button display graph fragment for exercise progress
        graphBtn.setOnClickListener{
            val fragment: Fragment = LineGraphFragment.newInstance(param1.toString())
            val fragmentManager =
                (context as FragmentActivity).supportFragmentManager.beginTransaction()
            fragmentManager.replace(R.id.contentLayout, fragment, "BARCHART").commit()
        }
    }

    // attempt to add an instance of selected exercise to selected date
    fun addExerciseInstance() {
        try {
            // try pass parameters to API end point
            val payload = mapOf("username" to user, "date" to sessionDate, "exercise" to param1)
            val r = post("http://192.168.0.15:8000/api/add_exercise_instance/", data = payload)
            // if successful remove current fragment and reload diary with new instance added
            if (r.statusCode == 200) {
                activity?.supportFragmentManager?.beginTransaction()?.remove(this)?.commit()
                val fragment: Fragment = DiaryFragment()
                val fragmentManager =
                    (context as FragmentActivity).supportFragmentManager.beginTransaction()
                fragmentManager.replace(R.id.contentLayout, fragment, "DIARY").commit()
            } else {
                // if unsuccessful then alert user
                val builder = AlertDialog.Builder(context)
                with(builder)
                {
                    setTitle("Error Adding Exercise")
                    setMessage("Trouble contacting server, please try again later.")
                    setNegativeButton("Exit") { dialog, _ ->
                        dialog.cancel()
                    }
                    setPositiveButton("Retry") { dialog, _ ->
                        addExerciseInstance()
                    }
                    show()
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ExerciseInformationFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(exerciseName: String, exerciseImage: Bitmap, exerciseDescription: String,
        exerciseInstructions: String, exerciseType: List<String>) =
            ExerciseInformationFragment().apply {
                arguments = Bundle().apply {
                    // includes all requirements to display exercise information
                    param1 = exerciseName
                    param2 = exerciseImage
                    param3 = exerciseDescription
                    param4 = exerciseInstructions
                    param5 =  exerciseType.toString()
                }
            }
    }
}