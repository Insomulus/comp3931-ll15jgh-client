package com.example.overpowered

// set instance object
class SetInstance(
    var time: String = "",
    var weight: String = "",
    var resistance: String = "",
    var bodyWeight: String = "",
    var addedWeight: String = "",
    var reps: String = "",
    var instanceId: String = "",
    var setId: String = ""
) {
}