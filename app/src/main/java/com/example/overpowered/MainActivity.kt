package com.example.overpowered

import CreateSetFragment
import EditSetFragment
import android.Manifest
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.example.overpowered.Session.Companion.KEY_USERNAME
import com.example.overpowered.databinding.ActivityMainBinding
import khttp.post
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.concurrent.thread
import com.example.overpowered.Session.Companion.KEY_LATITUDE
import com.example.overpowered.Session.Companion.KEY_LONGITUDE


// the Main Activity which houses all fragments for the application
class MainActivity : AppCompatActivity() {

    // initiate viewBinding
    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!
    private val fm = supportFragmentManager
    lateinit var session: Session
    lateinit var inflater : View


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        // checks that the user is signed in
        session = Session(applicationContext)
        session.auth()
        // initialise session key and fragments
        val user: HashMap<String?, String?> = session.getAccountDetails()
        val searchFragment = ExerciseSearchFragment()
        val diaryFragment = DiaryFragment()
        val profileFragment = ProfileFragment()

        // launches the core fragment for daily diary
        fragmentManager(diaryFragment, "DIARY")

        Log.d("PRINTED", user[KEY_USERNAME].toString())
        Log.d("PRINTED", user[KEY_LATITUDE] + " " + user[KEY_LONGITUDE])

        // sets on click listeners for navigation
        binding.bottomNavigationView.setOnItemSelectedListener {
            when(it.itemId) {
                R.id.menuIconSearch -> fragmentManager(searchFragment, "SEARCH")
                R.id.menuIconHome -> fragmentManager(diaryFragment, "DIARY")
                R.id.menuIconProfile -> fragmentManager(profileFragment, "PROFILE")
        }
            true
        }

        // custom onclick listeners for floating action button that causes different actions too occur
        // depending ont he fragment the user is currently focused on
        floatingActionBTN.setOnClickListener {
            val diary: DiaryFragment? = supportFragmentManager.findFragmentByTag("DIARY") as DiaryFragment?
            if (diary != null && diary.isVisible) {
                fragmentManager(searchFragment, "SEARCH")
            }
            val search: ExerciseSearchFragment? = supportFragmentManager.findFragmentByTag("SEARCH") as ExerciseSearchFragment?
            if (search != null && search.isVisible) {
                fragmentManager(CustomExerciseFragment(), "CUSTOM")
            }
            val custom: CustomExerciseFragment? = supportFragmentManager.findFragmentByTag("CUSTOM") as CustomExerciseFragment?
            if (custom != null && custom.isVisible) {
                custom.saveEdit()
            }
            val addSet: CreateSetFragment? = supportFragmentManager.findFragmentByTag("CREATESET") as CreateSetFragment?
            if (addSet != null && addSet.isVisible) {
                addSet.saveEdit()
            }
            val editSet: EditSetFragment? = supportFragmentManager.findFragmentByTag("EDITSET") as EditSetFragment?
            if (editSet != null && editSet.isVisible) {
                editSet.saveEdit()
            }
            val exerciseInformation: ExerciseInformationFragment? = supportFragmentManager.findFragmentByTag("EXERCISEINFO") as ExerciseInformationFragment?
            if (exerciseInformation != null && exerciseInformation.isVisible) {
                exerciseInformation.addExerciseInstance()
            }

            val noExercise: DiaryNoExercise? = supportFragmentManager.findFragmentByTag("NOEXERCISE") as DiaryNoExercise?
            if (noExercise != null && noExercise.isVisible) {
                fragmentManager(searchFragment, "SEARCH")
            }

        }

        // requests permissions on successful login
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.MANAGE_EXTERNAL_STORAGE,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ), 1
            )
        }
    }


    // fragment manager function to easily swap between fragments
    fun fragmentManager(fragment: Fragment, tag: String) {
        supportFragmentManager.beginTransaction().apply {
            supportFragmentManager.popBackStack()
            replace(R.id.contentLayout, fragment, tag).commit()
        }
    }

    // launches a calendar to pick a date from
    fun showCalendarDialog(v: View) {
        val newCalendarFragment = DatePickerFragment()
        newCalendarFragment.show(fm, "calendar")
    }


    // attempt to logout
    fun logoutRequest() {
        println("Attempting to logout")
        thread {
            try {
                val user: HashMap<String?, String?> = session.getAccountDetails()
                val payload = mapOf("username" to user[KEY_USERNAME], "password" to "")
                val r = post("http://192.168.0.15:8000/api/log_out/", data = payload)
                // if successful, logout and end session, redirect use to Login Activity
                if (r.statusCode == 200) {
                    session.endSession()
                    val intent = Intent(applicationContext, LoginActivity::class.java)
                    startActivity(intent)
                } else if (r.statusCode == 401) {
                    // if user logout fails on the server, still end session and redirect
                    session.endSession()
                    val intent = Intent(applicationContext, LoginActivity::class.java)
                    startActivity(intent)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }
}







